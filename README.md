<div align=center>
 
<img align="center" alt="GIF" src="https://github.com/manojuppala/manojuppala/blob/master/assets/code.gif?raw=true" width="750" height="520" /> 

## Introduction
 
I was born on March 3, 1998.🎂

Graduated from [@NYUST](https://www.yuntech.edu.tw/).

Major in CSIE. :man_technologist:

## 𝗧𝗲𝗰𝗸 𝗦𝘁𝗮𝗰𝗸

 
 
 
  <table>
   <tbody>
     <tr valign="top">
       <td width="20%" align="center">
         <span>Angular</span><br><br><br>
           <img height="64px" src="https://cdn.svgporn.com/logos/angular.svg">
       </td>
       <td width="20%" align="center">
         <span>JavaScript</span><br><br><br>
          <img height="64px" src="https://cdn.svgporn.com/logos/javascript.svg">
       </td>
       <td width="20%" align="center">
         <span>TypeScript</span><br><br><br>
         <img height="64px" src="https://cdn.svgporn.com/logos/typescript.svg">
       </td>
       <td width="20%" align="center">
         <span>HTML5</span><br><br><br>
         <img height="64px" src="https://cdn.svgporn.com/logos/html-5.svg">
       </td>
       <td width="20%" align="center">
         <span>CSS3</span><br><br><br>
         <img height="64px" src="https://cdn.svgporn.com/logos/css-3.svg">
       </td>
     </tr> 
      <tr valign="top">
       <td width="20%" align="center">
         <span>RxJS</span><br><br><br>
           <img height="64px" src="https://cdn.svgporn.com/logos/reactivex.svg">
       </td>
       <td width="20%" align="center">
         <span>Firebase</span><br><br><br>
          <img height="64px" src="https://cdn.svgporn.com/logos/firebase.svg">
       </td>
       <td width="20%" align="center">
         <span>Heroku</span><br><br><br>
         <img height="64px" src="https://cdn.svgporn.com/logos/heroku.svg">
       </td>
        <td width="20%" align="center">
         <span>postgreSQL</span><br><br><br>
         <img height="64px" src="https://cdn.svgporn.com/logos/mongodb.svg">
       </td>
       <td width="20%" align="center">
         <span>postgreSQL</span><br><br><br>
         <img height="64px" src="https://cdn.svgporn.com/logos/nodejs-icon.svg">
       </td>
     </tr> 
   </tbody>
 </table>
 


</div> 



© 2022 GitHub, Inc.
Terms
Privacy
Security
Status
Docs
Contact GitHub
Pricing
API
Training
Blog
About
1
